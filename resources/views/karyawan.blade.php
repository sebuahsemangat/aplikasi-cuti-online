@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Data Karyawan</div>

                <div class="card-body">
                <a href="/karyawan/create"><button class="btn btn-success btn-md mb-3">Tambah Data</button></a>
                    <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th style="width: 10px">#</th>
                            <th>NIK</th>
                            <th>Nama Karyawan</th>
                            <th>Jabatan</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @forelse ($karyawan as $key => $karyawans)
                        <tr>
                            <td>{{$key +1}}</td>
                            <td>{{$karyawans ->nik}}</td>
                            <td>{{$karyawans ->nama}}</td>
                            <td>{{$karyawans ->jabatan}}</td>
                            <td>
                            <form onsubmit="return confirm('Hapus data karyawan ini?')" method="POST" action="/karyawan/delete/{{$karyawans->nik}}">
                            @csrf
                            @method('delete')
                                <a href="/karyawan/{{$karyawans->nik}}/edit" ><button class="btn btn-warning btn-sm">Edit</button></a>
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                            </td>
                        </tr> 
                        @empty
                        <tr>
                            <td colspan="5">Belum ada data karyawan</td>
                        </tr>   
                        @endforelse
                            
                        </tbody>
                    </table> 
                <div class="row justify-content-center">{{$karyawan->links()}}</div>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection
