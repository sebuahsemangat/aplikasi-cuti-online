@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Data Pengajuan</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th style="width: 10px">#</th>
                            <th>Nama Karyawan</th>
                            <th>Alasan</th>
                            <th>Jenis Cuti</th>
                            <th>Tanggal</th>
                            <th>Bukti</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                            @forelse ($pengajuan as $key => $pengajuans)
                            <tr>
                                <td>{{ $key +1 }}</td>
                                <td>{{$pengajuans->karyawan->nama}}</td>
                                <td>{{$pengajuans->alasan}}</td>
                                <td>{{$pengajuans->jenis_cuti}}</td>
                                <td>
                                    <strong>{{$pengajuans->mulai}}</strong>
                                    <br>
                                    S.d.
                                    <br>
                                    <strong>{{$pengajuans->selesai}}</strong>
                                </td>
                                <td>
                                    <a href="{{asset('bukti/'.$pengajuans->bukti)}}" target="_blank">Lihat Bukti</a>
                                </td>
                                <td>
                                    @switch($pengajuans->status)
                              @case('Rejected')
                                  <span class="badge bg-danger" style="color: rgb(0, 0, 0)">Rejected</span>
                                  @break
 
                              @case('Approved')
                                  <span class="badge bg-success" style="color: white">Approved</span>
                                  @break
 
                              @default
                                  <span class="badge bg-warning" style="color: black">Pengajuan</span>
                            @endswitch
                                </td>
                                <td>
                                @if ($pengajuans->status==='Pengajuan')
                                <a href="/approve/{{$pengajuans->id}}" onclick="return confirm('Approve pengajuan cuti ini?')"><button class="btn btn-success btn-sm">Approve</button></a>
                                <a href="/reject/{{$pengajuans->id}}" onclick="return confirm('Reject pengajuan cuti ini?')"><button class="btn btn-danger btn-sm">Reject</button></a>
                                    
                                @else
                                Sudah Ditinjau 
                                @endif
                                    </td>
                                
                              </tr>    
                            @empty
                            <tr>
                                <td colspan="8">Belum ada pengajuan cuti</td>
                            </tr>
                            @endforelse
                          
                          
                        </tbody>
                    </table> 
                <div class="row justify-content-center">{{ $pengajuan->links() }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
