@extends('layouts.app')

@section('content')
<div class="container">
    @forelse ($karyawan as $item)
    <div class="row justify-content-center">
        
      <div class="col-md-6">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Ajukan Cuti</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="/inputpengajuan" method="POST" enctype="multipart/form-data">
              @csrf
              <div class="card-body">
                <input type="hidden" value="{{$item->nik}}" name="nik">
                <div class="form-group">
                  <label for="alasan">Alasan Cuti</label>
                  <input required type="text" name="alasan" class="form-control" id="alasan" placeholder="Masukan alasan cuti anda">
                  @error('alasan')
                      {{$message}}
                  @enderror
                </div>
                <div class="form-group">
                  <label>Tanggal Mulai:</label>
                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                      <input required type="date" class="form-control" name="mulai" />
                    </div>
                </div>
                <div class="form-group">
                  <label>Tanggal Berakhir:</label>
                  <div class="input-group date" id="reservationdate" data-target-input="nearest">
                      <input required type="date" class="form-control" name="selesai" />
                    </div>
                </div>
                <div class="form-group">
                  <label>Jenis Cuti:</label>
                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                        <select class="form-control" name="jenis_cuti">
                            <option>Cuti Tahunan</option>
                            <option>Cuti Besar</option>
                            <option>Cuti Sakit</option>
                            <option>Cuti Melahirkan</option>
                            <option>Cuti Karena Alasan Penting</option>
                            <option>Cuti Bersama</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Bukti Pendukung</label>
                  <div class="input-group">
                    <div class="custom-file">
                      <input required type="file" class="custom-file-input" id="exampleInputFile" name="bukti">
                      <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                    </div>
                  </div>
                </div>
                
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Ajukan Cuti</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col (left) -->
      <!-- right column -->
      <div class="col-md-6">
          <!-- Form Element sizes -->
          <div class="card card-success">
            <div class="card-header">
              <h3 class="card-title">Data Diri Anda</h3>
            </div>
            <div class="card-body">
              NIK: <strong>{{$item->nik}}</strong>
              <br>
              Nama: <strong>{{$item->nama}}</strong>
              <br>
              Jabatan: <strong>{{$item->jabatan}}</strong>
              
            </div>
            <!-- /.card-body -->
          </div>

          <div class="card card-success mt-2">
              <div class="card-header">
                <h3 class="card-title">Riwayat Cuti</h3>
              </div>
              <div class="card-body">
                  <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">#</th>
                          <th>Alasan</th>
                          <th>Tanggal</th>
                          <th style="width: 40px">Status</th>
                          <th style="width: 40px">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @forelse ($pengajuan as $key => $dpengajuan)
                        <tr>
                          <td>{{$key+1}}</td>
                          <td>{{$dpengajuan->alasan}}</td>
                          <td>
                            {{$dpengajuan->mulai}}
                            <br>
                            s.d.
                            <br>
                            {{$dpengajuan->selesai}}
                          </td>
                          <td>
                            @switch($dpengajuan->status)
                              @case('Rejected')
                                  <span class="badge bg-danger" style="color: rgb(0, 0, 0)">Rejected</span>
                                  @break
 
                              @case('Approved')
                                  <span class="badge bg-success" style="color: white">Approved</span>
                                  @break
 
                              @default
                                  <span class="badge bg-warning" style="color: black">Pengajuan</span>
                            @endswitch
                          <td>
                            @if ($dpengajuan->status==='Pengajuan')
                            <form action="/pengajuan/{{$dpengajuan->id}}/{{$dpengajuan->karyawan->nik}}" method="POST" onsubmit="return confirm('Batalkan Pengajuan Cuti?')">
                              @csrf
                              @method('delete')  
                              <button title="Batalkan Pengajuan" type="submit" class="btn btn-xs btn-danger">X</button>
                            </form>
                            @else
                            Sudah ditinjau
                            @endif
                              
                          </td>  
                        </tr> 
                        @empty
                        <tr>
                          <td colspan="4">
                            Anda belum mengajukan cuti.
                          </td>
                        </tr>
                        @endforelse
                        
                        
                      </tbody>
                    </table>                       
              </div>
              <!-- /.card-body -->
            </div>
          <!-- /.card -->
        </div> 
  </div>
    @empty
    <div class="row justify-content-center">
      <div class="col-md-8">
          <div class="card">
              <div class="card-header">Pengajuan Cuti Online</div>

              <div class="card-body">
                  <form method="GET" action="/carinik">
                      @csrf

                      <div class="form-group row">
                          <label for="nik" class="col-md-4 col-form-label text-md-right">Masukan NIK</label>

                          <div class="col-md-6">
                              <input id="nik" type="number" class="form-control" name="search" value="" required autofocus>
                          </div>
                      </div>
                      <div class="form-grup row justify-content-center">
                          
                          <span class="alert alert-danger">
                            NIK tidak ditemukan. Silahkan coba lagi!
                          </span>
                      </div>
                      <div class="form-group row mb-0">
                          <div class="col-md-8 offset-md-4">
                              <button type="submit" class="btn btn-primary">
                                  Cari NIK
                              </button>
                          </div>
                      </div>
                  </form>
              </div>
              
          </div>
      </div>
  </div>
    @endforelse
</div>
@endsection
