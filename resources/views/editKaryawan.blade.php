@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Data Karyawan</div>

                <div class="card-body">
                  @if(session('success'))
                    <p class="alert alert-success">
                        {{session('success')}} <a href="/karyawan/data"> Lihat data</a>
                    </p> 
                  @endif
                    <form action="/karyawan/update/{{$karyawan->nik}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                          
                          <div class="form-group">
                            <label for="alasan">NIK Karyawan</label>
                            <input disabled value="{{old('nik',$karyawan->nik)}}" required type="number" name="nik" class="form-control" placeholder="Masukan NIK Karyawan">
                        
                          </div>
                          <div class="form-group">
                            <label>Nama Karyawan</label>
                            <input value="{{old('nik',$karyawan->nama)}}" required type="text" name="nama" class="form-control" placeholder="Masukan Nama Karyawan">
                        
                          </div>
                          <div class="form-group">
                            <label>Jabatan</label>
                            <input value="{{old('nik',$karyawan->jabatan)}}" required type="text" name="jabatan" class="form-control" placeholder="Masukan Jabatan Karyawan">
                        
                          </div>
                          <div class="form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                          </div>
          
                      </form>   
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection
