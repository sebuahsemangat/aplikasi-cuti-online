<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/approve/{id}', 'PengajuanController@approve');
Route::get('/reject/{id}', 'PengajuanController@reject');

Route::get('/karyawan/data', 'KaryawanController@index');
Route::get('/karyawan/create', 'KaryawanController@create');
Route::post('/karyawan/store', 'KaryawanController@store');
Route::get('/karyawan/{id}/edit', 'KaryawanController@edit');
Route::put('/karyawan/update/{nik}', 'KaryawanController@update');
Route::delete('/karyawan/delete/{nik}', 'KaryawanController@destroy');
});


Route::get('/', function () {
    return view('welcome');
});

Route::get('/carinik', 'KaryawanController@carinik');
Route::post('/inputpengajuan', 'PengajuanController@store');
Route::delete('/pengajuan/{id}/{nik}', 'PengajuanController@destroy');
Auth::routes();
