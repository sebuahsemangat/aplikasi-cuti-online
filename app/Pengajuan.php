<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Karyawan;
class Pengajuan extends Model
{
    protected $table = "pengajuans";
    protected $primaryKey = 'id';
    protected $fillable = ["alasan", "mulai", "selesai", "jenis_cuti", "bukti", "status", "karyawans_nik"];
    public function karyawan (){
        return $this->belongsTo('App\Karyawan', 'karyawans_nik', 'nik');
    }
    
}
