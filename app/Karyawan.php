<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
Use App\Pengajuan;
class Karyawan extends Model
{
    protected $table = "karyawans";
    protected $primary ="nik";
    protected $fillable = ["nik", "nama", "jabatan"];
    public function pengajuan(){
        return $this->hasMany('App\Pengajuan', 'karyawans_nik');
    }
}
