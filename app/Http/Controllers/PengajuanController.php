<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Pengajuan;

class PengajuanController extends Controller
{
    public function store (Request $request)
    {
     $bukti = $request->bukti;
     $bukti_baru = time() . '-' . $bukti->getClientOriginalName();
    
     $pengajuan = new Pengajuan;
     $pengajuan->alasan = $request->alasan;
     $pengajuan->mulai = $request->mulai;
     $pengajuan->selesai = $request->selesai;
     $pengajuan->jenis_cuti = $request->jenis_cuti;
     $pengajuan->bukti = $bukti_baru;
     $pengajuan->status = 'Pengajuan';
     $pengajuan->karyawans_nik = $request->nik;
     
     $pengajuan->save();

     $bukti->move('bukti/', $bukti_baru);

     return redirect('/carinik?search='.$request->nik);
    }

    public function approve ($id, Request $request) {
        $pengajuan = Pengajuan::find($id);
        $pengajuan->status = 'Approved';
        $pengajuan->update();
        return redirect('/home');
    }
    public function reject ($id, Request $request) {
        $pengajuan = Pengajuan::find($id);
        $pengajuan->status = 'Rejected';
        $pengajuan->update();
        return redirect('/home');
    }
    public function destroy($id, $nik, Request $request){
        $pengajuan=Pengajuan::find($id);
        $pengajuan->delete();
        return redirect('/carinik?search='.$nik);
        
    }
}
