<?php

namespace App\Http\Controllers;
Use DB;
use Illuminate\Http\Request;
use App\Karyawan;
use App\Pengajuan;

class KaryawanController extends Controller
{
    public function carinik(Request $request) {
        $nik = $request->search;
        $karyawan = Karyawan::where('nik', '=', $nik)->paginate(1);
        $pengajuan = Pengajuan::where('karyawans_nik', '=', $nik)->orderBy('id', 'desc')->paginate(5);
        //mencari data berdasarkan $nik
        return view ('pengajuan',compact('karyawan','pengajuan'), [
            
        ]);
    }

    public function index()
    {
        $karyawan = Karyawan::paginate(10);
        return view('karyawan', compact('karyawan'));
    }
    public function create()
    {
        return view('formKaryawan');
    }
    public function store(Request $request)
    {
          
        $karyawan = new Karyawan;
        $karyawan->nik = $request->nik;
        $karyawan->nama = $request->nama;
        $karyawan->jabatan = $request->jabatan;
                
        $karyawan->save();
   
          
        return redirect('/karyawan/data');  
    }
    public function edit($id)
    {
        $karyawan = DB::table('karyawans')->where('nik', $id)->first();
        return view ('editKaryawan', compact('karyawan'));
    }
    public function update($nik, Request $request)
    {
        $query = DB::table('karyawans')
                    ->where('nik', $nik)
                    ->update([
                        'nama' => $request['nama'],
                        'jabatan' => $request['jabatan']
                    ]);
        return redirect ('/karyawan/'.$nik.'/edit')->with('success','Data karyawan berhasil diubah.');
    }
    public function destroy($nik, Request $request){
        $query = DB::table('karyawans')
        ->where('nik', $nik)
        ->delete();

        return redirect ('/karyawan/data');
    }
}
