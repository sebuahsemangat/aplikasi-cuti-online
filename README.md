<h1>Aplikasi Cuti Online</h1>
<p>Dibuat Oleh Apep Wahyudin</p>

<h4>Alur Aplikasi</h4>
<ol>
    <li>Manager memasukkan data karyawan berupa NIK, Nama, dan Jabatan</li>
    <li>Karyawan memasukkan NIK pada halaman utama. Kemudian klik tombol cari NIK</li>
    <li>Karyawan memasukkan data pengajuan cuti berupa Alasan, tanggal mulai, tanggal selesai, jenis cuti, dan upload bukti pendukung.</li>
    <li>Manager melihat data pengajuan dan dapat memilih tombol Approve atau Reject untuk menerima atau menolak pengajuan cuti.</li>
    <li>Hasil tinjauan dari manager akan muncul ketika karyawan memasukkan NIK pada halaman utama</li>
</ol>

<h4>Login Manager</h4>
<ol>
    <li>Klik tombol Login Manager di bagian atas</li>
    <li>Manager login menggunakan e-mail dan password yang telah didaftarkan</li>
    <li>Manager dapat mengelola data karyawan dan data pengajuan</li>
</ol>

<h4>Pendaftaran Akun Manager</h4>
<ol>
    <li>Akses url: localhost:8000/register</li>
    <li>Isi data yang diminta</li>
</ol>
<br>
<p>Special thanks to Muhammad Ridwan Fauzi yang sudah menjelaskan konsep ORM sampai saya paham.</p>