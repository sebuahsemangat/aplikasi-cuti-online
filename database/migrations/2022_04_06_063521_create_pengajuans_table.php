<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengajuansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('alasan');
            $table->date('mulai');
            $table->date('selesai');
            $table->string('jenis_cuti');
            $table->string('bukti');
            $table->string('status');
            $table->string('karyawans_nik');
            $table->foreign('karyawans_nik')->references('nik')->on('karyawans')->onDelete('cascade');;
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuans');
    }
}
